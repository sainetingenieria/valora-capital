(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 935,
	height: 630,
	fps: 30,
	color: "#FFFFFF",
	manifest: [
		{src:"images/banner1.png", id:"banner1"},
		{src:"images/banner2.png", id:"banner2"},
		{src:"images/banner3.png", id:"banner3"},
		{src:"images/banner4.png", id:"banner4"},
		{src:"images/banner5.png", id:"banner5"},
		{src:"images/banner6.png", id:"banner6"},
		{src:"images/banner7.png", id:"banner7"}
	]
};

// stage content:
(lib.banner = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Superior
	this.instance = new lib.clipsuperior();
	this.instance.setTransform(358.3,384,1,1,0,0,0,304.2,236.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(450));

	// Capa 7
	this.instance_1 = new lib.img7("synched",0);
	this.instance_1.setTransform(463.5,279.5,1,1,0,0,0,463.5,279.5);
	this.instance_1.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({alpha:0},4).wait(400).to({startPosition:0},0).to({alpha:1},10).wait(60).to({startPosition:0},0).to({alpha:0.488},5).wait(1));

	// Capa 6
	this.instance_2 = new lib.img6("synched",0);
	this.instance_2.setTransform(463.5,279.5,1,1,0,0,0,463.5,279.5);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(350).to({_off:false},0).to({alpha:1},10).wait(60).to({startPosition:0},0).to({alpha:0},10).to({_off:true},1).wait(35));

	// Capa 5
	this.instance_3 = new lib.img5("synched",0);
	this.instance_3.setTransform(463.5,279.5,1,1,0,0,0,463.5,279.5);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(299).to({_off:false},0).to({alpha:1},10).wait(60).to({startPosition:0},0).to({alpha:0},10).to({_off:true},1).wait(70));

	// Capa 4
	this.instance_4 = new lib.img4("synched",0);
	this.instance_4.setTransform(463.5,279.5,1,1,0,0,0,463.5,279.5);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(224).to({_off:false},0).to({alpha:1},10).wait(60).to({startPosition:0},0).to({alpha:0},10).to({_off:true},1).wait(145));

	// Capa 3
	this.instance_5 = new lib.img3("synched",0);
	this.instance_5.setTransform(463.5,279.5,1,1,0,0,0,463.5,279.5);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(149).to({_off:false},0).to({alpha:1},10).wait(60).to({startPosition:0},0).to({alpha:0},10).to({_off:true},1).wait(220));

	// Capa 2
	this.instance_6 = new lib.img2("synched",0);
	this.instance_6.setTransform(463.5,279.5,1,1,0,0,0,463.5,279.5);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(74).to({_off:false},0).to({alpha:1},10).wait(60).to({startPosition:0},0).to({alpha:0},10).to({_off:true},1).wait(295));

	// Capa 1
	this.instance_7 = new lib.img1("synched",0);
	this.instance_7.setTransform(463.5,279.5,1,1,0,0,0,463.5,279.5);
	this.instance_7.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({alpha:1},9).wait(60).to({startPosition:0},0).to({alpha:0},10).to({_off:true},1).wait(370));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(467.5,315,927,559);


// symbols:
(lib.banner1 = function() {
	this.initialize(img.banner1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,927,559);


(lib.banner2 = function() {
	this.initialize(img.banner2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,927,559);


(lib.banner3 = function() {
	this.initialize(img.banner3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,927,559);


(lib.banner4 = function() {
	this.initialize(img.banner4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,927,559);


(lib.banner5 = function() {
	this.initialize(img.banner5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,927,559);


(lib.banner6 = function() {
	this.initialize(img.banner6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,927,559);

(lib.banner7 = function() {
	this.initialize(img.banner7);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,927,559);

(lib.img7 = function() {
	this.initialize();

	// Capa 1
	this.instance = new lib.banner7();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,927,559);

(lib.img6 = function() {
	this.initialize();

	// Capa 1
	this.instance = new lib.banner6();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,927,559);


(lib.img5 = function() {
	this.initialize();

	// Capa 1
	this.instance = new lib.banner5();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,927,559);


(lib.img4 = function() {
	this.initialize();

	// Capa 1
	this.instance = new lib.banner4();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,927,559);


(lib.img3 = function() {
	this.initialize();

	// Capa 1
	this.instance = new lib.banner3();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,927,559);


(lib.img2 = function() {
	this.initialize();

	// Capa 1
	this.instance = new lib.banner2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,927,559);


(lib.img1 = function() {
	this.initialize();

	// Capa 1
	this.instance = new lib.banner1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,927,559);


(lib.Path_2 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#000000","#7F7F7F"],[0,1],73.4,-165.2,-96.9,72.5).s().p("EggvALLQGCogG1okQJJreJdqOQH/AQISgwQI/g0Iyh8QwOPqvDS2QrLOCogNJQmzoknwrHg");
	this.shape.setTransform(209.7,197.4);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,419.4,394.9);


(lib.Path_1 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#72100F","#DC0932"],[0.408,1],103.5,-187.1,-66.2,49.8).s().p("AydC7QGLq0ITrEQK3DVLmBNQnwI7npJ1QlrHXlLHSQkxm/l7pEg");
	this.shape.setTransform(118.2,121.4);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,236.5,242.9);


(lib.Path = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#000000","#7F7F7F"],[0,1],110,-180.1,-59.8,56.8).s().p("Aq4j/QCFkSDHksQH0EiIxDUQjgE/jMFPQicEBiBD1Qk3nolxpUg");
	this.shape.setTransform(69.8,83);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,139.6,166);


(lib.clipsuperior = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_29 = function() {
		/* Detener en este fotograma
		La línea de tiempo se detendrá/pausará en el fotograma en el que se inserte este código.
		También se puede utilizar para detener/pausar la línea de tiempo de clips de película.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(29).call(this.frame_29).wait(1));

	// <Path>
	this.instance = new lib.Path();
	this.instance.setTransform(-0.7,0,0.068,0.068,0,0,0,-0.8,0);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:-0.8,alpha:0.898},9).wait(21));

	// <Path>_1
	this.instance_1 = new lib.Path_1();
	this.instance_1.setTransform(94.7,66.1,0.034,0.034,0,0,0,1.5,0);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(9).to({_off:false},0).to({regX:0.8,regY:0.1,scaleX:1,scaleY:1,alpha:0.898},10).wait(11));

	// <Path>_2
	this.instance_2 = new lib.Path_2();
	this.instance_2.setTransform(189.4,78.7,0.096,0.096,0,0,0,0.5,0.5);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(19).to({_off:false},0).to({regY:0.4,scaleX:1,scaleY:1,x:189.5,y:78.8,alpha:0.898},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.7,0,9.5,11.3);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;