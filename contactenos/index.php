<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Days+One' rel='stylesheet' type='text/css'>

<link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
<script src=" https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js" type="text/javascript"></script>
<script src="jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
		
				function valida_envia(){
					
					if(document.fvalida.nombre.value.length==0){
						alert("Por Favor escriba su nombre");
						document.fvalida.nombre.focus();
						return false;
					}
					
					
					if(document.fvalida.email.value.indexOf('@')==-1){
					alert("La direccion de email no es correcta");
					document.fvalida.email.focus();
					return false;
					}
					
					if(document.fvalida.mensaje.value.length==0){
						alert("Por Favor escriba su mensaje");
						document.fvalida.mensaje.focus();
						return false;
					}
					
					
					
					document.fvalida.submit();
					
					
				}
</script>

<style>


* {
	color: #000;
	text-align: left;
	font-family: 'Days One', sans-serif;
	font-weight: normal;
	font-size: 15px;
    text-transform: uppercase;
}

.error { color:#C00;  }

em { color:#C00; }

input[type='text']{ 
    background-color: #F2F2F2;
    border: 0;
    font-size: 14px;
    font-family: Arial, Helvetica, sans-serif;
    height: 23px;
    font-weight:normal;
    margin-bottom: 1px;
    margin-left: 1px;
    padding-bottom: 3px;
    text-align: left;
    width: 363px;
    color:#999;
    margin-top: -8px;
    -webkit-box-shadow: 0 17px 9px -22px rgba(0, 0, 0, 0.8) inset;
    -moz-box-shadow: 0 17px 9px -22px rgba(0, 0, 0, 0.8) inset;
    box-shadow: 0 17px 9px -22px rgba(0, 0, 0, 0.8) inset;
    text-transform: none;
}

textarea{
    background-color: #F2F2F2;
    border: 0;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 14px;
    font-weight:normal;
    margin-bottom: 1px;
    margin-left: 1px;
    padding-bottom: 1px;
    text-align: left;
    width: 363px;
    height: 70px; !important;
    overflow:auto;
    color:#999;
    margin-top: -18px;
    -webkit-box-shadow: 0 17px 9px -22px rgba(0, 0, 0, 0.8) inset;
    -moz-box-shadow: 0 17px 9px -22px rgba(0, 0, 0, 0.8) inset;
    box-shadow: 0 17px 9px -22px rgba(0, 0, 0, 0.8) inset;
    text-transform: none;
}

#tel, #ciudad {width: 171px;}

input[type='submit']{  background-color: transparent;
    background-image: url("bt.png");
    background-repeat: no-repeat;
    border: medium none;
    color:#fff;
    cursor: pointer;
    font-family: 'Open Sans', sans-serif;
    height:28px;
    padding-top: 0;
    text-align: center;
    width:133px;
    margin-top: -8px;
    margin-left: -4px;
    margin-bottom: 5px;
    text-transform: none;
}
    
input[type='submit']:hover {opacity:0.7;}


input[type='reset']{ background-color: transparent;
    background-image: url("bt.png");
    background-repeat: no-repeat;
    border: medium none;
    color:#fff;
    cursor: pointer;
    float: left;
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    height: 28px;
    padding-top: 0;
    text-align: center;
    width: 133px;
    margin-top: -8px;
    margin-left: 102px;
    text-transform: none;
}
    
input[type='reset']:hover {opacity:0.7;}

#commentForm { }

/*#commentForm label { width:114px; height:10px;}

#commentForm label.error { margin-left: 6px; }*/


</style>

<body>
<form id="commentForm"  name="fvalida" method="post" action="procesar.php" onSubmit="return valida_envia()">
<table style="width:368px; height:359px;" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2"><label for="cname">Nombre:</label ></td>
  </tr>
  <tr>
    <td colspan="2"><input name="nombre" type="text" id="cname" minlength="2" /></td>
  </tr>
  <tr>
    <td colspan="2"><label for="cemail">Email:</label></td>
  </tr>
  <tr>
    <td colspan="2"><input name="email" type="text" id="cemail" class="required" /></td>
  </tr>
  <tr>
    <td><label>Teléfono</label></td>
    <td style="padding-left: 14px;"><label>Ciudad</label></td>
  </tr>
  <tr>
    <td><input onkeypress="return justNumbers(event);" name="tel" type="text" id="tel" class="required"  /></td>
    <td style="padding-left: 14px;"><input name="ciudad" type="text" id="ciudad" class="required"  /></td>
  </tr>
  <tr>
    <td colspan="2"><label>Mensaje</label></td>
  </tr>
  <tr>
    <td colspan="2"><textarea name="mensaje" cols="20" rows="3" id="mensaje" class="required"></textarea></td>
  </tr>
  <tr>
    <td colspan="2"><input type="reset" value=" Borrar"/><input type="submit" name="Submit" value="Enviar"  /></td>
  </tr>
</table>
</form>
</body>
</head>
</html>

